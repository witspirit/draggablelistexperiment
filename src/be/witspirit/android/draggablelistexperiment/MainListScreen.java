package be.witspirit.android.draggablelistexperiment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainListScreen extends Activity {

    private static final String TAG = MainListScreen.class.toString();
    
    private List<String> content;

    private ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list_screen);

        content = generateContent(100);

        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, content);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick("+parent+", "+view+", "+position+", "+id+")");
                clicked(id);
            }
        });

        Button insertButton = (Button) findViewById(R.id.insert);
        insertButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                insertItem();
            }
        });

        Button removeButton = (Button) findViewById(R.id.remove);
        removeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem();
            }});

        Button shuffleButton = (Button) findViewById(R.id.shuffle);
        shuffleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                shuffle();
            }
        });

    }
    
    protected void insertItem() {
        Log.d(TAG, "insertItem()");
        content.add(0, "New Inserted Item");
        adapter.notifyDataSetChanged();
    }
    
    protected void shuffle() {
        Log.d(TAG, "shuffle()");
        Collections.shuffle(content);
        adapter.notifyDataSetChanged();
    }

    protected void removeItem() {
        Log.d(TAG, "removeItem()");
        content.remove(0);
        adapter.notifyDataSetChanged();
    }

    private void clicked(long id) {
        Log.d(TAG, "clicked("+id+")");
        content.set((int) id, "Item "+id+" - Clicked");
        adapter.notifyDataSetChanged();
    }

    private List<String> generateContent(int nrOfItems) {
        List<String> content = new ArrayList<String>();
        for (int i = 0; i < nrOfItems; i++) {
            content.add("Item " + i);
        }
        return content;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_list_screen, menu);
        return true;
    }
}
